---
title: Start to Code

language_tabs: # must be one of https://git.io/vQNgJ
  - python

toc_footers:
  - A project in support of <a href='https://engineeringgood.org/'>Engineering Good</a>
  - <a href='https://github.com/slatedocs/slate'>Documentation Powered by Slate</a>

includes:
  - languages

search: true

code_clipboard: true
---

# Start to Code

So you've heard about coding, you've seen people writing code which looks like arcane magic spells,
and you want to start learning to code, but don't know where to start.

This tutorial will demystify the basics of coding for you, open doors for you to learn more,
and show you some real-world usage of coding.

This tutorial is designed to supplement the excellent book "[Automating the Boring Stuff with Python](https://automatetheboringstuff.com/)".

## Why do you want to code?

The answer to this question determines what language you want to learn. Personally, I would recommend spending a short time in Python to build up your fundamentals.

[Why are there so many programming languages?](#why-are-there-so-many-programming-languages)

### I want to get a coding job
Refer to Carl Cheo's excellent [infographic](http://carlcheo.com/startcoding). If you are brand new to coding, this tutorial is a good quick start before you dive into more specialised languages.

### I have a startup idea
Refer to Carl Cheo's excellent [infographic](http://carlcheo.com/startcoding). If you are brand new to coding, this tutorial will give you the skills to prototype and test your ideas.

### I want to learn together with my children
[Scratch](https://scratch.mit.edu) is a language designed for young children. If you are brand new to coding, this tutorial will give you a good foundation and help you solve problems in any language, including Scratch.

### I'm not sure, just what can coding do for me?
Read on!

## What can coding do for me?

Coding can help you in many everyday tasks.

### Fill in a Google Form automatically
- For example, automate health declarations

### Monitor websites for changes and collect data / notify
- Automatically collect weather/transport/forex/stock-market data

### Work with many documents/files/folders
- Find all instances of name/address/email/phone in Word/Excel documents
- Rename all Word documents to their first sentence

### Control your keyboard and mouse
- Automatically navigate websites, click links, fill in forms

### Automatically process images
- Find and highlight faces in images
- Apply overlay / Transform images

# How to start coding?

<%= image_tag "images/love_hate_programming.jpg" %>

## Fundamentals
These are the basics of coding that any programming course will bring you through.

- **Hello World**: How to code and run your first program: [Chapter 1](https://automatetheboringstuff.com/2e/chapter1/)
- **Flow Control**: How your program decides what to do / repeats actions: [Chapter 2](https://automatetheboringstuff.com/2e/chapter2/)
- **Functions, Modules**: How your program interacts with other things: [Chapter 3](https://automatetheboringstuff.com/2e/chapter3/)
- **Lists, Dictionaries**: How to handles lots of data: [Chapter 4](https://automatetheboringstuff.com/2e/chapter4/) & [Chapter 5](https://automatetheboringstuff.com/2e/chapter5/)
- **Strings & Regular Expressions**: How to handle text: [Chapter 6](https://automatetheboringstuff.com/2e/chapter6/) & [Chapter 7](https://automatetheboringstuff.com/2e/chapter7/)
- **Files**: How to save, read, write, move files: [Chapter 8](https://automatetheboringstuff.com/2e/chapter8/), [Chapter 9](https://automatetheboringstuff.com/2e/chapter9/) & [Chapter 10](https://automatetheboringstuff.com/2e/chapter10/)

<aside class="notice">
These fundamentals may seem daunting at first glance.
However, the best way to start coding is to complete these fundamental chapters,
doing all the hands-on coding tasks and seeing the results for yourself.
No matter what programming tasks you take on,
these fundamentals are as essential as water is to life.
</aside>

## Behind the scenes
Instead of going through a typical programming course, let's go in a reverse direction - starting with an existing application, and examining the important points that a programmer would notice. Once the important points are identified, we can write simple programs that can control the application.

In this way, this tutorial will quickly show you coding in a real life example.

### How does Google Maps work?

Let's start with a commonly used application - Google Maps

<%= image_tag "images/maps_url.png" %>

Typically, you would use Google Maps like this:

1. You enter Destination address, click search
2. You click directions
3. You enter From address, press enter

Try these steps for yourself, and observe the changes in the address bar:

1. You enter destination address, click search
   - Browser address bar changes to `google.com/maps/place/your+address/some_other_stuff`

2. You click directions
   - Browser address bar changes to `google.com/maps/dir//your+address/some_other_stuff`

3. You enter from address, press enter
   - Browser address bar changes to `google.com/maps/dir/from+address/to+address/some_other_stuff`

### How does a programmer see this?

- The content in the browser address bar is called a [URL](https://en.wikipedia.org/wiki/Uniform_Resource_Locator). URLs are split by slashes "/". We can study each element between the slashes to see how Google Maps works.
- Using the URL prefix `google.com/maps/place` or `google.com/maps/dir`, we can directly access Google Maps place and direction without clicking buttons.
- Spaces " " are converted into plus signs "+" in the URL. Spaces usually need special handling in programs, and here Google replaces them with "+" signs.
- It is usually possible to remove unneeded information from the URL after any slash. Looking at the URL, it should be possible to only have the from_address and to_address.

Let's test this out - try copying and pasting this URL in your browser address bar:

`https://google.com/maps/dir/201+Henderson+Road/65+Kerbau+Road/`

This should directly show you the directions from Engineering Good's old office in Sprout Hub (201 Henderson Road) to the new office in Little India (65 Kerbau Road).

### Automating Google Maps

> Basic Python program that opens Google Maps with directions

```python
import webbrowser

webbrowser.open('https://google.com/maps/dir/201+Henderson+Road/65+Kerbau+Road/')
```

With this understanding of Google Maps, we can use Python to write a program that opens Google Maps with these directions loaded.

In python, the `webbrowser` [module](https://automatetheboringstuff.com/2e/chapter3/) can be used to open a web browser to a given URL.

To use a module in our program, we have to `import` it at the beginning of the program.

The `webbrowser` module has a [function](https://automatetheboringstuff.com/2e/chapter3/) called `open` which opens the web browser.

To pass the URL to the `open` function, we will need to place it in quotes 'like this'.

### Errors during programming

> Common error seen in this program

```shell
webbrowser.open(https://google.com/maps/dir/)
                     ^
SyntaxError: invalid syntax
```

It is normal to see errors during programming. However, the errors are usually difficult to interpret without experience. In the display to the right, you will see a typical error `SyntaxError`. Syntax refers to the rules of the programming language -- in this case, you have written something that does not follow the rules of the language. What's missing is the 'quotation marks' at the start and the end of the URL.

<aside class="notice">
Why are quotation marks needed? This is because of the difference between a [variable](https://automatetheboringstuff.com/2e/chapter1/) and a [string](https://automatetheboringstuff.com/2e/chapter1/). These are covered in [Chapter 1](https://automatetheboringstuff.com/2e/chapter1/) of the book "Automate the Boring Stuff with Python".
</aside>

### Separating the URL into three strings

> Separating into three strings for later processing

```python
import webbrowser

google_maps_dir = 'https://google.com/maps/dir/'
from_address = '201+Henderson+Road'
to_address = '65+Kerbau+Road'

webbrowser.open(google_maps_dir + from_address + '/' + to_address)
```

The URL is a [string](https://en.wikipedia.org/wiki/String_(computer_science)) formed from three strings joined by slashes '/'.
Using variables, we can separate these into three parts: the Google Maps part, the From address, and the To address.

This example shows how we can join these strings together into a URL for the webbrowser to open.

### Using code to replace the spaces with +

> Using code to replace the spaces with +

```python
import webbrowser

google_maps_dir = 'https://google.com/maps/dir/'
from_address = '201 Henderson Road'
from_address = from_address.replace(' ','+')
to_address = '65 Kerbau Road'
to_address = to_address.replace(' ','+')

webbrowser.open(google_maps_dir + from_address + '/' + to_address)
```

Google Maps expects a '+' instead of spaces in addresses. Replacing the spaces with + in addresses can be tedious, so let's use the string replace function to replace all occurrences of space ' ' with plus '+' in the from and to addresses.

### Using lists and loops to repeatedly process data

> Using lists and loops to allow the computer to do much more

```python
import webbrowser

volunteer_addresses = [
  'Block 999A Buangkok Crescent',
  'Block 111 Ang Mo Kio Ave 4',
  'Block 888 Tampines Street 81'
]

google_maps_dir = 'https://google.com/maps/dir/'
to_address = '65 Kerbau Road'
to_address = to_address.replace(' ','+')

for address in volunteer_addresses:
  from_address = address
  from_address = from_address.replace(' ','+')
  webbrowser.open(google_maps_dir + from_address + '/' + to_address)

```

You now need to generate directions to help volunteers find their way to Engineering Good from all over Singapore. Using a list of addresses, and using a "for loop", you can now have your program go through the volunteer addresses, and for every volunteer address, open a new page in the browser with the correct directions.

### Additional uses for Google Maps

You want to pull the volunteer information from Excel? - openpyxl module

You want to send WhatsApp messages with the Google Maps directions? - pywhatkit module

## Other use cases - Work-in-Progress

When considering automation - think about these levels of automation.

- Keyboard/Mouse → Website/App → API? → Database
- Control Keyboard/Mouse - SikuliX http://www.sikulix.com/
- Control Website - Python + BS4
- Control API - Python + JSON or find app-specific modules

Higher level automation is usually slower, but more easy to design and build. For example, game bots.

Lower level automation (e.g. API) can be run really quickly, but needs more investigation and study.

Use case: Print custom seating cards (more than mail merge)

Websites: client and server
- Client speaks HTTP to server
- Advanced - Use Web Inspector and "Network" tab to see the interactions

Documents: Word, Excel, PDF, CSV

## Automation ethics and courtesy

Automation is a risk to websites and services:

- Too fast / too many requests which jam up the server and block other people from using it.
- Game bots - using computers to gain advantage over other players without bots - makes other players feel as if the high levels are unreachable - reduced motivation to play.
- Client tracking - websites like to be able to keep track of which clients are accessing them - they may block you if you do not behave like a human client.
- One request every 5-10 seconds is reasonable.


# Read more
Based off “Automate the Boring Stuff with Python”
Read Chapter 0 to 11: until Debugging. You will need all of it.
Chapter 12 to 19: Specific examples, read as you need.
Chapter 20 GUI automation: Don’t need. I recommend SikuliX, easier.
Appendix A: Third Party Modules
Appendix B: Running Programs: When you want to run a program every day/week, read this.

