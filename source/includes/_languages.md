# Why are there so many programming languages?

## Arcane computer wizardry

- Punch cards
- Assembly
- COBOL
- FORTRAN
- BASIC
- DOS
- C

## Object Oriented Programming - Computer space describing real world objects

- C#
- C++
- Java

## Internet - Clients and servers

- HTML
- CSS
- Javascript
- PHP
- Perl

## Web 2.0 - Powerful clients

- Javascript (AJAX)
- Ruby / Rails

## Programming for everyone!

- Python
- Go
- Sketch
